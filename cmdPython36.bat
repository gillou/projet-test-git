@echo off
REM Passage en mode UTF-8
chcp 65001
REM Lancement d'un terminal avec les paths Python :
REM 1: Choix de la version
set VERSION=3.6.8
set PATH=C:\Python\%VERSION%;C:\Python\%VERSION%\Scripts;%PATH%

REM : rappel syntaxe création environnement virtuel
echo. && echo Pour creer un environnement virtuel : python -m venv D:\Projets\Client\Projet\Dev\venv_projet && echo.
echo. && echo Installation recommandées : pip install ipython jupyter pylint && echo.

REM 2: Lancement de la console
@cmd.exe

