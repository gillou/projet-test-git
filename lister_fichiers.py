# encoding: utf-8

import os
import glob

# Liste des fichiers images des sous-dossiers
liste_fichiers = glob.glob('images/*/*.jpg')

# Boucle for sur le séléments de la liste
for fichier in liste_fichiers:
    # Extraction des champs du nom de fichier : Promo Prénom_nom 
    champs_fichiers = fichier.split(sep=os.sep)
    promo = champs_fichiers[1]
    # Récupère le nom de fichier avec l'extension
    nom_prenom = os.path.basename(fichier)
    # Enlève l'extension
    nom_prenom = os.path.splitext(nom_prenom)[0]
    # Affichage pour Vérification
    print('Promo : {}, Nom : {}'.format(promo,nom_prenom))


