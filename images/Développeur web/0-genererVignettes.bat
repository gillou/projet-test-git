REM @echo off
:: Création du dossier des vignettes s'il n'existe pas
IF NOT EXIST thumbs (
	mkdir thumbs
)

setlocal EnableDelayedExpansion
:: Boucle sur tous les fichiers d'extensions .jpg du dossier
for %%a in (*.jpg) do (
	:: Test si le fichier existe déjà
	IF NOT EXIST "thumbs/%%a" (
		:: Redimensionnement avec l'outil imagemagick
		convert -define jpeg:size=600x1000 "%%a" -auto-orient -thumbnail 300x500 -unsharp 0x.5 "thumbs/%%a"
	) ELSE (
		echo "thumbs/%%a : la vignette existe"
	)
)
pause
 