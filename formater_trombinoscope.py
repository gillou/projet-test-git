# coding: utf-8

import os
import glob

# Liste des fichiers images des sous-dossiers
liste_fichiers = glob.glob('images/*/*.jpg')

# Initialisation 
strHtmlGrid = u''
strHtmlBoot = u''

# Boucle for sur le séléments de la liste
for fichier in liste_fichiers:
    # Extraction des champs du nom de fichier : Promo Prénom_nom 
    champs_fichiers = fichier.split(sep=os.sep)
    promo = champs_fichiers[1]
    tag_promo = promo.split(sep=' ')[0].lower()
    tag_promo = tag_promo.replace('é', 'e')

    # Récupère le nom de fichier avec l'extension
    nom_prenom_ext = os.path.basename(fichier)

    # Enlève l'extension
    nom_prenom = os.path.splitext(nom_prenom_ext)[0]

    # Formate les cartes HTML format Grid Flex
    strHtmlGrid = strHtmlGrid + '''
    <a href="images/{1}/{2}" class="{0}">
        <figure>
            <img src="images/{1}/thumbs/{2}" alt="{3}" class="image">
            <figcaption class="{0}">
                {3}
            </figcaption>
        </figure>
    </a>
    '''.format(tag_promo, promo, nom_prenom_ext, nom_prenom)

    # Formate les cartes HTML format Bootstrap
    strHtmlBoot = strHtmlBoot + '''
        <div class="col-lg-3 col-md-4 col-6">
            <div class="carte {0}">
                <a href="images/{1}/{2}" class="d-block mb-4 h-100">
                    <img class="img-fluid img-thumbnail" src="images/{1}/thumbs/{2}" alt="{3}">
                    <div class="overlay {0}">{3}</div>
                </a>
            </div>
        </div>    '''.format(tag_promo, promo, nom_prenom_ext, nom_prenom)


# Ecriture dans la console
print(strHtmlGrid)


# Écriture dans un fichier
import codecs

# Format Flex
file_writer = codecs.open('trombi_grid_temp.html','w', 'utf-8')
file_writer.write(strHtmlGrid)
file_writer.close()

# Format Bootstrap
file_writer = codecs.open('trombi_boot_temp.html','w', 'utf-8')
file_writer.write(strHtmlBoot)
file_writer.close()
