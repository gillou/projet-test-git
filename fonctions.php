<?php 
/** Fonction renvoyant un tableau de la liste des cris d'un dossier 
  * Les cris doivent être organisés en dossier ou en dossier/sous-dossier
  * Le tableau de la liste des cris est rangé dans l'ordre croissant dans le temps de sauvegarde
  */

function listerCris($basedir) {
	// Extension recherchée
	$ext = "_thumb.jpg";
	// Fichiers associés
	$extReq = array(".mp3", ".jpg", ".mp4");
	// Initialisation de la sortie
	$liste = array();

	// Renvoie la liste des fichiers recherchés s'il y en a
	// echo $basedir.'/*'.$ext;
	$lFichiers = glob ($basedir.'/*'.$ext); // , GLOB_NOSORT);

  // Renvoie une liste de fichiers pour un repertoire et une extension donnee
  foreach ($lFichiers as $fic) {
		$mp3name = str_replace($ext, ".mp3", $fic);
		$jpgname = str_replace($ext, ".jpg", $fic);
		$mp4name = str_replace($ext, ".mp4", $fic);
		//echo '<br />'.$fic.' '.$mp3name.' '.$jpgname.' '.$mp4name;
    //if (file_exists($mp3name) && file_exists($jpgname) && file_exists($mp4name)) {
		if (file_exists($jpgname)) {
			$date = filemtime($fic);
      $liste[$fic] = $date;
			// echo '<br />'.$fic.' date : '.date('l jS \of F Y h:i:s A', $date);
    }
  }
	
	// Tri par date croissante
  // arsort($liste);
  //echo '<br />'.count($liste).' fichiers trouves';     

  return $liste;
}

function formatterCrisJSON($basedir) {
	$strJSON = "[\n";
	$ext = "_thumb.jpg";
	
	// Recherche des sous-répertoires 
	$listDir = glob($basedir.'/*', GLOB_ONLYDIR);
	if (count($listDir)>0) {
		// Boucle sur les sous-répertoires
		foreach($listDir as $dir) {
			$listeFic = listerCris($dir);
			arsort($listeFic);
			if (count($listeFic)>0) {
				foreach ($listeFic as $fic => $date) {
					$fic     = str_replace("//", "/", $fic);
					$jpgname = str_replace($ext, "", $fic);
					$strJSON = $strJSON."{\"nom\":\"$jpgname\"},";
				}
			}
		}
	} else {
		// Pas de sous-répertoire : créé la liste pour le répertoire courant
		$listeFic = listerCris($basedir); 
		arsort($listeFic);
		$section = basename($basedir);
		    
		if (count($listeFic)==0) {
			return '<h2>Le r&eacute;pertoire demand&eacute; ne contient pas (encore) de cris, d&eacute;sol&eacute;.</h2>';
		}

	  foreach ($listeFic as $fic => $date) {
					$fic     = str_replace("//", "/", $fic);
					$jpgname = str_replace($ext, "", $fic);
					$strJSON = $strJSON."{\"nom\":\"$jpgname\"},";
		}
	}

	$strJSON = substr($strJSON,0,-1)."]";
	return $strJSON;
}

// $basedir : répertoire à scanner, avec un '/' à la fin
function formatterCrisHtml($basedir) {
	$strHtml = '';
	$ext = "_thumb.jpg";
	
	// Recherche des sous-répertoires 
	$listDir = glob($basedir.'/*', GLOB_ONLYDIR);
	if (count($listDir)>0) {
		// Première boucle table des matières 
		$strHtml = $strHtml.'<ul>';
		foreach($listDir as $dir) {
			$listeFic = listerCris($dir);
			if (count($listeFic)>0) {
				// Lien vers le sous-répertoire
				$section = basename($dir);
				$name = str_replace('-', '_', $section);
		    $strHtml = $strHtml.'<li><a href="#'.$name.'">'.str_replace('_', ' ', $section).'</a></li>'."\n";
			}
		}
		$strHtml = $strHtml.'</ul>';

		// 2eme boucle image par section
		foreach($listDir as $dir) {
			$listeFic = listerCris($dir);
			if (count($listeFic)>0) {
				// Titre H2 avec le nom du sous-répertoire
				$section = basename($dir);
				$name = str_replace('-', '_', $section);
		    
		    $strHtml = $strHtml.'<h2 id="'.$name.'">'.str_replace('_', ' ', $section).' ('.count($listeFic).' images) - <a href="#top">&#10548;</a> </h2>';
				$strHtml = $strHtml."\n"."<div class=\"container\">\n <div class=\"imgSet\">";
				foreach ($listeFic as $fic => $date) {
					$fic     = str_replace("//", "/", $fic);
					$mp3name = str_replace($ext, ".mp3", $fic);
					$jpgname = str_replace($ext, ".jpg", $fic);
					$printname = str_replace($ext, "_print.jpg", $fic);
					if (file_exists($printname)) { $jpgname = $printname; }
						
					$mp4name = str_replace($ext, ".mp4", $fic);
					$title = basename($fic, $ext);
					if (file_exists($mp3name)) {
						$strHtml = $strHtml."\n  <a class=\"imgLink\" href=\"$jpgname\" data-lightbox=\"$section\"\n     ".
			    		"data-title=\" sauver <a href='$jpgname'> image</a> | <a href='$mp4name'> vid&eacute;o mp4</a> | ".
							"<a href='$mp3name'> son mp3</a><audio src='$mp3name' preload='none' autoplay='autoplay'></audio>\">\n     ".
			    		"<img class=\"imgBig\" src=\"$fic\" alt=\"$title.jpg\"/></a>&nbsp;\n";
					} else {
						$strHtml = $strHtml."\n  <a class=\"imgLink\" href=\"$jpgname\" data-lightbox=\"$section\"\n     ".
			    		"data-title=\" sauver <a href='$jpgname'> image</a> | son mp3 non disponible\">\n     ".
			    		"<img class=\"imgBig\" src=\"$fic\" alt=\"$title.jpg\"/></a>&nbsp;\n";
					}
				}
			}
		}
	} else {
		// Pas de sous-répertoire : créé la liste pour le répertoire courant
		$listeFic = listerCris($basedir); 
		$section = basename($basedir);
		    
		if (count($listeFic)==0) {
			return '<h2>Le r&eacute;pertoire demand&eacute; ne contient pas (encore) de cris, d&eacute;sol&eacute;.</h2>';
		}

	  foreach ($listeFic as $fic => $date) {
					$fic     = str_replace("//", "/", $fic);
					$mp3name = str_replace($ext, ".mp3", $fic);
					$jpgname = str_replace($ext, ".jpg", $fic);
					$printname = str_replace($ext, "_print.jpg", $fic);
					if (file_exists($printname)) { $jpgname = $printname; }
					$mp4name = str_replace($ext, ".mp4", $fic);
					$title = basename($fic, $ext);
					/*$strHtml = $strHtml."\n  <a class=\"imgLink\" href=\"$jpgname\" data-lightbox=\"$section\"\n     ".
		    		"data-title=\" sauver <a href='$jpgname'> image</a> | <a href='$mp4name'> vid&eacute;o mp4</a> | ".
						"<a href='$mp3name'> son mp3</a><audio src='$mp3name' preload='none' autoplay='autoplay'></audio>\">\n     ".
		    		"<img class=\"imgBig\" src=\"$fic\" alt=\"$title.jpg\"/></a>&nbsp;\n";*/
					if (file_exists($mp3name)) {
						$strHtml = $strHtml."\n  <a class=\"imgLink\" href=\"$jpgname\" data-lightbox=\"$section\"\n     ".
			    		"data-title=\" sauver <a href='$jpgname'> image</a> | <a href='$mp4name'> vid&eacute;o mp4</a> | ".
							"<a href='$mp3name'> son mp3</a><audio src='$mp3name' preload='none' autoplay='autoplay'></audio>\">\n     ".
			    		"<img class=\"imgBig\" src=\"$fic\" alt=\"$title.jpg\"/></a>&nbsp;\n";
					} else {
						$strHtml = $strHtml."\n  <a class=\"imgLink\" href=\"$jpgname\" data-lightbox=\"$section\"\n     ".
			    		"data-title=\" sauver <a href='$jpgname'> image</a> | son mp3 non disponible\">\n     ".
			    		"<img class=\"imgBig\" src=\"$fic\" alt=\"$title.jpg\"/></a>&nbsp;\n";
					}
		}
	}

	return $strHtml;
}

// $basedir : répertoire à scanner, avec un '/' à la fin
function formaterPhotosHtml($basedir) {
	$strHtml = '';
	$ext = "_thumb.jpg";
	
	// Pas de sous-répertoire : créé la liste pour le répertoire courant
	$listeFic = listerCris($basedir); 
	$section = basename($basedir);
	    
	if (count($listeFic)==0) {
		return '<h2>Le r&eacute;pertoire demand&eacute; ne contient pas (encore) de photos, d&eacute;sol&eacute;.</h2>';
	} else {
		echo '<p>'.count($listeFic).' photos</p>';
	}

	foreach ($listeFic as $fic => $date) {
		$fic     = str_replace("//", "/", $fic);
		$screenname = str_replace($ext, "_screen.jpg", $fic);
		$jpgname = str_replace($ext, ".jpg", $fic);
		$title = basename($fic, $ext);
		$strHtml = $strHtml."\n  <a class=\"imgLink\" href=\"$screenname\" data-lightbox=\"$section\"\n     ".
			"data-title=\" sauver <a href='$jpgname'> image</a>\">\n ".
			"<img class=\"imgBig\" src=\"$fic\" alt=\"$title.jpg\"/></a>&nbsp;\n";
		}

	return $strHtml;
}


// Renvoie le nombre de cris pour un répertoire (et ses sous-répertoires)
function compterCris($basedir) {
	$nbCris = 0;
	// Recherche des sous-répertoires 
	$listDir = glob($basedir.'/*', GLOB_ONLYDIR);
	if (count($listDir)>0) {
		// Boucle sur les sous-répertoires
		foreach($listDir as $dir) {
			$listeFic = listerCris($dir);
			$nbCris += count($listeFic);
		}
	} else {
		// Pas de sous-répertoire : créé la liste pour le répertoire courant
		$listeFic = listerCris($basedir); 
		$nbCris = count($listeFic);		
	}

	return $nbCris;

}

// Renvoie une liste formatée pour Diaporama
function formatterCrisDiaporama($basedir) {
	$strHtml = '';
	$ext = "_thumb.jpg";
	
	// Recherche des sous-répertoires 
	$listDir = glob($basedir.'/*', GLOB_ONLYDIR);
	if (count($listDir)>0) {
		// Boucle sur les sous-répertoires
		foreach($listDir as $dir) {
			$listeFic = listerCris($dir);
			arsort($listeFic);
			if (count($listeFic)>0) {
				foreach ($listeFic as $fic => $date) {
					$fic     = str_replace("//", "/", $fic);
					$jpgname = str_replace($ext, ".jpg", $fic);
					$strHtml = $strHtml."\n  <div>\n    <img u=\"image\" src=\"$jpgname\"/>\n".
			    		"    <img u=\"thumb\" src=\"$fic\"/>\n  </div>   ";
				}
			}
		}
	} else {
		// Pas de sous-répertoire : créé la liste pour le répertoire courant
		$listeFic = listerCris($basedir); 
		arsort($listeFic);
		$section = basename($basedir);
		    
		if (count($listeFic)==0) {
			return '<h2>Le r&eacute;pertoire demand&eacute; ne contient pas (encore) de cris, d&eacute;sol&eacute;.</h2>';
		}

	  foreach ($listeFic as $fic => $date) {
					$fic     = str_replace("//", "/", $fic);
					$jpgname = str_replace($ext, ".jpg", $fic);
					$strHtml = $strHtml."\n  <div>\n    <img u=\"image\" src=\"$jpgname\"/>\n".
			    		"    <img u=\"thumb\" src=\"$fic\"/>\n  </div>   ";					
		}
	}

	return $strHtml;

}

function formatterCrisSimpleHtml($basedir) {
	$strHtml = '';
	
	// Pas de sous-répertoire : créé la liste pour le répertoire courant
	$listeFic = listerCris($basedir); 
	$section = basename($basedir);
	   
	if (count($listeFic)==0) {
		return '<h2>Le r&eacute;pertoire demand&eacute; ne contient pas (encore) de cris, d&eacute;sol&eacute;.</h2>';
	}


	$strHtml = $strHtml."\n  <ul class=\"galerie\">\n";
	foreach ($listeFic as $fic => $date) {
	  $strHtml = $strHtml.formaterCrisLi($fic, $section);
	}
	$strHtml = $strHtml."\n  </ul>\n";

	return $strHtml;
	
}

function formaterCrisLi($fic, $section) {
	$fic     = str_replace("//", "/", $fic);
	$ext = "_thumb.jpg";
	$mp3name = str_replace($ext, ".mp3", $fic);
	$jpgname = str_replace($ext, ".jpg", $fic);
	$mp4name = str_replace($ext, ".mp4", $fic);
	$title = basename($fic, $ext);
	if (file_exists($mp3name)) {
		$strHtml = "\n  <li><a class=\"imgLink\" href=\"$jpgname\" data-lightbox=\"$section\"\n     ".
		"data-title=\"<audio src='$mp3name' preload='none' autoplay='autoplay'></audio>\">\n     ".
		"<img class=\"imgBig\" src=\"$fic\" alt=\"$title.jpg\"/></a></li>\n";
	} else {
		$strHtml = "\n  <li><a class=\"imgLink\" href=\"$jpgname\" data-lightbox=\"$section\"\n     ".
		"data-title=\"$title.jpg\"/></a></li>\n";
	}	
	return $strHtml;
	
}


// Renvoie une liste vers les vignettes d'un nombre aléatoire de cris
function listerVignettesCrisAleatoires($nbCris) {
	$listDir = glob('./images/*', GLOB_ONLYDIR);
}

?>

